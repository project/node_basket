<?php

/**
 * @file
 * Node basket module.
 *
 * This little module allows people to pick up nodes in a basket (currently
 * session-stored), so that custom operations can then be performed on these
 * nodes.
 *
 * @author Gaël Gosset, 2013
 *
 * TODO :
 * - Add ajax behavior to avoid page reloads
 * - If user is logged in, we could store the basket data in the $user object
 * - Better hook for operations
 */

/**
 * Implements hook_node_view().
 */
function node_basket_node_view($node, $view_mode, $langcode) {
  if (user_access('pick up nodes')) {
    $basket = node_basket_get_content();
    if (!empty($basket) && in_array($node->nid, array_keys($basket))) {
      $node->content['links']['node_basket_state'] = array(
        '#theme' => 'link',
        '#text' => t('In your basket'),
        '#path' => 'node-basket',
        '#options' => array(
          'html' => FALSE,
          'attributes' => array(
            'class' => array('node-basket-state'),
            'title' => t('This content is in your basket.'),
          ),
        ),
      );

      $node->content['links']['node_basket_throw_out'] = array(
        '#theme' => 'link',
        '#text' => t('Throw out'),
        '#path' => 'node/' . $node->nid . '/throw-out',
        '#options' => array(
          'html' => FALSE,
          'query' => drupal_get_destination(),
          'attributes' => array(
            'class' => array('node-basket-throw-out-link'),
            'title' => t('Throw this content out from your basket.'),
          ),
        ),
      );

    }
    else {
      $node->content['links']['node_basket_pick_up'] = array(
        '#theme' => 'link',
        '#text' => t('Pick up'),
        '#path' => 'node/' . $node->nid . '/pick-up',
        '#options' => array(
          'html' => FALSE,
          'query' => drupal_get_destination(),
          'attributes' => array(
            'class' => array('node-basket-pick-up-link'),
            'title' => t('Pick this content up to your basket.'),
          ),
        ),
      );
    }
  }
}

/**
 * Implements hook_menu().
 */
function node_basket_menu() {
  $items = array();
  $items['node/%node/pick-up'] = array(
    'page callback' => 'node_basket_pick_up',
    'page arguments' => array(1),
    'access callback' => 'node_basket_access_callback',
    'access arguments' => array(
      'pick up',
      1,
    ),
    'type' => MENU_CALLBACK,
  );
  $items['node/%node/throw-out'] = array(
    'page callback' => 'node_basket_throw_out',
    'page arguments' => array(1),
    'access callback' => 'node_basket_access_callback',
    'access arguments' => array(
      'throw out',
      1,
    ),
    'type' => MENU_CALLBACK,
  );
  $items['node-basket'] = array(
    'title' => 'Basket',
    'page callback' => 'node_basket_page',
    'access arguments' => array('pick up nodes'),
    'type' => MENU_SUGGESTED_ITEM,
  );
  foreach (module_invoke_all('node_basket_operations') as $key => $op) {
    if (function_exists($op['callback'])) {
      $item = array(
        'page callback' => 'node_basket_operation',
        'page arguments' => array($op['callback']),
        'type' => MENU_CALLBACK,
      );
      if (isset($op['perm'])) {
        $item['access arguments'] = array($op['perm']);
      }
      else {
        $item['access callback'] = TRUE;
      }
      $items['node-basket/' . $key] = $item;
    }
  }
  return $items;
}

/**
 * Implements hook_permission().
 */
function node_basket_permission() {
  return array('pick up nodes' => array(
      'title' => t('pick up nodes'),
      'description' => t('Add nodes to the basket'),
    ), );
}

/**
 * Implements hook_block_info().
 */
function node_basket_block_info() {
  $blocks['basket'] = array(
    'info' => t('Node basket'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function node_basket_block_view($delta) {
  if ($delta == 'basket' && user_access('pick up nodes')) {
    $block = array(
      'subject' => t('My basket'),
      'content' => node_basket_display_block_basket(),
    );
  }
  return $block;
}

/**
 * Implements hook_theme().
 */
function node_basket_theme($existing, $type, $theme, $path) {
  return array('node_basket_page' => array('variables' => array(
        'rows' => array(),
        'links' => array(),
      ), ), );
}

/**
 * Implements hook_node_basket_operations().
 * It's more an example on how to use the hook, as nids are not really needed
 * to flush the basket.
 */
function node_basket_node_basket_operations() {
  $ops = array();
  $ops['flush'] = array(
    'text' => t('Flush basket'),
    'perm' => 'pick up nodes',
    'callback' => 'node_basket_flush',
  );

  return $ops;
}

/**
 * Executes an operation onto the listed nodes.
 */
function node_basket_operation($func) {
  $nodes = node_basket_get_content();
  return $func($nodes);
}

/**
 * Operation function which flush the basket.
 */
function node_basket_flush($nodes) {
  foreach ($nodes as $node) {
    unset($_SESSION['node_basket_nids'][$node->nid]);
  }
  drupal_set_message(t('Your basket has been flushed.'));
  return drupal_goto(isset($_GET['destination']) ? $_GET['destination'] : 'node-basket');
}

/**
 * Adds the node to the user's basket.
 */
function node_basket_pick_up($node) {
  // We store the basket data in the sessions table : anonymous user
  // identification will remain according to session.cookie_lifetime
  // (provided that the user doesn't flush the session cookie in its navigator)
  // and session data will be deleted according to session.gc_maxlifetime.
  // These can be set in the settings.php file.
  $_SESSION['node_basket_nids'][$node->nid] = REQUEST_TIME;

  drupal_set_message(t('%title has been added to your basket.', array('%title' => $node->title)));

  return drupal_goto($_GET['destination'] ? $_GET['destination'] : 'node/' . $nid);
}

/**
 * Removes the node from the user's basket.
 */
function node_basket_throw_out($node) {
  unset($_SESSION['node_basket_nids'][$node->nid]);

  drupal_set_message(t('%title has been removed from your basket.', array('%title' => $node->title)));

  return drupal_goto($_GET['destination'] ? $_GET['destination'] : 'node/' . $nid);
}

/**
 * Returns the content of the basket as an array keyed with nids.
 */
function node_basket_get_content() {
  if (empty($_SESSION['node_basket_nids'])) {
    return array();
  }

  asort($_SESSION['node_basket_nids'], SORT_NUMERIC);
  $nids = array_keys($_SESSION['node_basket_nids']);

  // We avoid use of node_load but will only get basic info.
  $query = db_select('node', 'n');
  $query->addTag('node_access');
  $query->fields('n');
  $query->condition('nid', array($nids), 'IN');
  $result = $query->execute();
  $rows = $result->fetchAllAssoc('nid', PDO::FETCH_OBJ);

  // Second loop to have a timestamp-ordered list.
  $nodes = array();
  foreach ($nids as $nid) {
    $node = $rows[$nid];
    if (is_object($node)) {
      $nodes[$nid] = $node;
    }
  }
  return $nodes;
}

/**
 * Checks if the user is allowed to do an action for a node.
 */
function node_basket_access_callback($action, $node) {
  return (node_access('view', $node) && user_access('pick up nodes'));
}

/**
 * Returns the basket block content.
 */
function node_basket_display_block_basket() {
  $text = format_plural(count(node_basket_get_content()), '1 item', '@count items');
  return l($text, 'node-basket', array('attributes' => array(
      'alt' => t('Basket'),
      'title' => t('Basket')
    )));
}

/**
 * Returns the page which lists the nodes in the basket.
 */
function node_basket_page() {
  $nodes = node_basket_get_content();
  if (empty($nodes)) {
    return t('Your basket is empty.');
  }
  $rows = array();
  foreach ($nodes as $node) {
    $cells = array(
      $node->title,
      l(t('View'), 'node/' . $node->nid),
      l(t('Throw out'), 'node/' . $node->nid . '/throw-out', array('query' => drupal_get_destination())),
    );
    $rows[] = $cells;
  }

  $links = array();
  foreach (module_invoke_all('node_basket_operations') as $key => $op) {
    if (!isset($op['perm']) || user_access($op['perm'])) {
      $links[] = l($op['text'], 'node-basket/' . $key);
    }
  }
  return theme('node_basket_page', array(
    'rows' => $rows,
    'links' => $links
  ));
}

/**
 * Theme function, returns the basket page.
 */
function theme_node_basket_page($variables) {
  $nodes_table = theme('table', array(
    'header' => array(),
    'rows' => $variables['rows']
  ));
  $item_list = theme('item_list', array('items' => $variables['links']));
  return $nodes_table . $item_list;
}
