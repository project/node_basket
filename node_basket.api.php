<?php
/**
 * @file
 * Hooks provided by the Node Basket module.
 */

/**
 * @addtogroup hooks
 * @{
 */
/**
 * Allow modules to add operations to be performed to the picked up nodes.
 *
 * @see node_basket_node_basket_operations()
 */
function hook_node_basket_operations() {
  $ops['my_operation_1'] = array(
    // This is the text of the link that will be added under your basket's list.
    'text' => t('Do 1'),
    // This is the callback function returning the HTML which should be printed
    // when clicking the link. It will be given one argument : an array
    // of node objects (those which are in the basket).
    'callback' => 'my_callback_1',
  );
  $ops['my_operation_2'] = array(
    'text' => t('Do 2'),
    'callback' => 'my_callback_2',
  );
  return $ops;
}

/**
 * @} End of "addtogroup hooks".
 */
