
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage
 
INTRODUCTION
------------

Current Maintainer: Gaël Gosset <ggosset@insite.coop>

This little module allows people to pick up nodes in a basket (currently
session-stored), so that custom operations can then be performed on these nodes.
The module only provides a flush operation, but a hook allows you to add custom
operations to your basket, such as a print operation, using Print module and 
the provided hook.

INSTALLATION
------------

* Install it as any module.
* New links will appear on node pages: pick up/throw out.
* A new block will be available on the blocks configuration page, displaying
the picked up nodes count and linking to the basket page.
* A menu item to the basket page can be enabled.
* The basket page will list the picked up nodes and provide links to perform
operations to the list.

USAGE
-----

* Use node_basket_get_content() to list the user's picked up nodes in your own
custom module.
* See node_basket_api.php for how to add operations to your node basket list.
